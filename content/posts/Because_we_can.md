---
title: Because we can
summary: On the Art of Code
author: CSDUMMI
date: 2022-06-25
categories:
- design
- programming
keywords:
- Art of Code
- Programming
- art
- code
- design
- corporate
- client
- user
---

For too long the attempt to make programming and software development a science
and software developers engineers has damaged our understanding of what it means
to develop software.

From waterfall methodologies, to extreme programming (XP) and even domain driven
design, the goal has been to lay out strict criteria and strict processes for
how software should be, nay must be.

And in all of these dogmas the advice given to developers has been to listen to
clients, to consider themselves the service providers to some nebulous client
and to heed their word as much as possible.

This view of software development as a business relationship has, of course,
been founded in corporate software development where this is the most immediate
form of software development.

But it is by far not the only one. If you have ever written a piece of code, not
because you wanted to please some client or customer, but because you were
excited about the prospect of creating that particular piece of software, then
you have already been engaging in a different sort of development.

For not all software development needs to be product and results focused, a lot
of it actually is written 'because we can' and for no reason beyond it. Let me
explain what that phrase actually means. By accepting that you want to build
something, just because you want to engage in the process of creation and not
for some 'customer' or 'client' - you are embracing software development as an
art.

As an art, software development has a force and power, that it can never have as
a tool for business automation.

It can be the expression of personality, impacting the minds and souls of it's
audience, changing their world and giving people all across the globe a common
humanity.

Use software as the artistic medium it clearly is, instead of a commercial
product it has been reduced to, and we shall find the true power of this new
medium.

